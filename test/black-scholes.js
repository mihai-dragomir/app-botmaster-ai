var expect = require('chai').expect;
let BSHolder = require('../services/BlackScholes.js').BSHolder;
var BS = require('../services/BlackScholes.js').BS;

describe("BlackScholes", function(){
  it('calls for stock = 57.42 and strike = 56', function () {

    // 1. ARRANGE
    var deltaReference = 57.42;
    var k = 56;
    var holder = new BSHolder(deltaReference,k,0.015,0.21,0.16);
    var deltaPercent = 0.64382;

    // 2. ACT
    var actualdeltaPercent = BS.cdelta(holder);

    // 3. ASSERT
    expect(actualdeltaPercent).to.be.equal(deltaPercent);
  });

});
