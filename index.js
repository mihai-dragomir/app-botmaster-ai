const ngrok = require('ngrok');
var rp = require('request-promise');

// Botmaster modules
const Botmaster = require('botmaster');
const TelegramBot = require('./services/botmaster-telegram')
const SessionWare = require('botmaster-session-ware');

/* 
  Botmaster Middlewares provides services that application uses, which are composed out of the functions.is about how our application 
  responds to incoming/outgoing requests. Middlewares look into the incoming and outgoing request, and make decisions based on this
  request. 
*/

const incomingMiddleware = require('./middleware/incoming');
const outgoingMiddleware = require('./middleware/outgoing');
const {fulfillOutgoingWare} = require('botmaster-fulfill');
const fulfillOutgoingActions = require('./util/botmaster-outgoing-actions');

// Configs
const {aixBotmasterSettings, telegramSettings} = require('./config');

// Express App
const expressServer = require('./api');

// Others
const {telegramBotWebhookUrlFn} = require('./util/telegram-helper');

const app = expressServer.express();
app.post('/trader/message',
  function (req, res) {
    botmaster.getBot({id: telegramBot.id}).sendMessageTo({
      text: req.body.message
    }, req.body.receiverId)
    .then((successMessage) => {
      res.send(successMessage);
    })
    .catch((error) => {
      res.status(error.statusCode).send(error);
    });
})

const server = app.listen(aixBotmasterSettings.port, '0.0.0.0', () => {
  console.log('BotmasterAi Server listening at port %d', aixBotmasterSettings.port);

  if(aixBotmasterSettings.env === 'local'){
    (async function() {

      const url = await ngrok.connect(parseInt(aixBotmasterSettings.port));

      // build the telegramWebhookUrl using bot authentication token, bot Webhook Endpoint and the local Url
      const telegramWebhookUrl = telegramBotWebhookUrlFn(
        telegramSettings.credentials.authToken,
        telegramSettings.webhookEndpoint,
        url);

      rp({uri: telegramWebhookUrl})
        .then((respons) => {
          console.log('Telegram Webhook was set: ' + telegramWebhookUrl);
        })
        .catch((err) => {
          console.log('Telegram Webhook setup error');
          console.log(err);
        })
    })();
  }
});

// Create Bot
const botmaster = new Botmaster({server});
const telegramBot = new TelegramBot(telegramSettings);

// Add an existing bot to this instance of botmaster
botmaster.addBot(telegramBot);

// Adding Incoming Botmaster Middlewares
botmaster.use(incomingMiddleware.hello.helloMiddleware);
botmaster.use(incomingMiddleware.aixBotApi.aixBotApiMiddleware);
botmaster.use(incomingMiddleware.debug.addDebugInfoToUpdate);
botmaster.use(incomingMiddleware.bug.saveBug);
botmaster.use(incomingMiddleware.userActions.addUserActions);
botmaster.use(incomingMiddleware.reply.replyToUser);

// Botmaster Outgoing Middlewares
botmaster.use(fulfillOutgoingWare(fulfillOutgoingActions));
botmaster.use(outgoingMiddleware.watsonPlaceholdersTransformers.replaceWatsonPlaceholders);

const sessionWare = new SessionWare();

// Wrapped middleware places the incoming middleware at beginning of incoming stack and the outgoing middleware at end of outgoing stack. 
botmaster.useWrapped(sessionWare.incoming, sessionWare.outgoing);

botmaster.on('error', (bot, err) => {
  console.log(err.message);
});
