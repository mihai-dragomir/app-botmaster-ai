if (process.env.NODE_ENV === 'local') {
    require('dotenv-safe').config();
}
const aixBotmasterSettings = require('./config-botmaster');
const telegramSettings = require('./config-telegram');
const aixBotApiSettings = require('./config-bot-api');

module.exports = {
  aixBotmasterSettings,
  telegramSettings,
  aixBotApiSettings,
}
