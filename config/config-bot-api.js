
const aixBotApiSettings = {
  AIX_BOT_API_DOMAIN: process.env.AIX_BOT_API_DOMAIN,
  AIX_BOT_API: process.env.AIX_BOT_API,
  AIX_BOT_MASTER_KEY: process.env.AIX_BOT_MASTER_KEY,
};

module.exports = aixBotApiSettings;
