const telegramSettings = {
  credentials: {
    authToken: process.env.TELEGRAM_TOKEN,
  },
  webhookEndpoint: '/webhook1234/',
  telegramBotUsername: process.env.TELEGRAM_BOT_USERNAME
};

module.exports = telegramSettings;
