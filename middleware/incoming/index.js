const hello = require('./hello');
const debug = require('./debug');
const bug = require('./bug');
const userActions = require('./user-actions');
const reply = require('./reply');
const aixBotApi = require('./aix-bot-api');

module.exports = {
  hello,
  debug,
  bug,
  userActions,
  reply,
  aixBotApi
};
