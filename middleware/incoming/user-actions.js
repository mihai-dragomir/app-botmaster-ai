const utils = require("../../utils.js");
var _ = require('lodash');

const addUserActions = {
  type: 'incoming',
  name: 'add-user-actions',
  controller: (bot, update, next) => {
    console.log("> Middleware - addUserActions");

    if(!update.apiResponse){
      next();
    }

    if(update.apiResponse.watsonUpdate && update.apiResponse.watsonUpdate.context && update.apiResponse.watsonUpdate.context.user_action){
      console.log("User action: " + update.apiResponse.watsonUpdate.context.user_action);
      switch (update.apiResponse.watsonUpdate.context.user_action) {
        case "put_or_call":
          update.outputMessage = update.outputMessage + "<buttons>Put|Call</buttons>";
          break;

        case "enter_quantity":
          update.outputMessage = update.outputMessage + "<buttons>5M|1M|500K</buttons>";
          break;

        case "bid_or_offer":
          update.outputMessage = update.outputMessage + "<buttons>Bid|Offer</buttons>";
          break;

        case 'yes_or_no':
          update.outputMessage = update.outputMessage + "<buttons>Yes|No</buttons>";
          break;

        default:
          break;
      }
    }

    next();
  }
};

module.exports = {
  addUserActions
}
