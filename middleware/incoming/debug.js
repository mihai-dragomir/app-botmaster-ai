const addDebugInfoToUpdate = {
  type: 'incoming',
  name: 'add-debug-info-to-update',
  controller: (bot, update, next) => {
    console.log("> Middleware - addDebugInfoToUpdate");

    console.log("> BOT INFO:");
    console.log(bot);

    console.log("> Update:");
    console.log(update);

    console.log("> Session:");
    console.log(update.session);

    next();
  }
};

module.exports = {
  addDebugInfoToUpdate,
}
