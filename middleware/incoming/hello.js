const utils = require("../../utils.js");
var _ = require('lodash');
var emoji = require('node-emoji');
var helper = require('../../util/telegram-helper');

const BOT_COMMANDS = utils.BOT_COMMANDS;

const helloMiddleware = {
  type: 'incoming',
  name: 'hello-middleware',
  controller: (bot, update, next) => {
    console.log("=====================================");
    console.log("> Middleware - hello -> " + update.sender.id);
    console.log(update);
    update.session.skip_bot_api = false;
    update.session.isGroupChat = false;

    let isGroupChat = helper.isGroupChat(update);

    if(isGroupChat) {
      update.session.isGroupChat = isGroupChat;
      update.session.skip_bot_api = true;
      next();
    }

    if(update.message.text == BOT_COMMANDS.START) {
      update.session.skip_bot_api = true;
      update.outputMessage = [
        'Hello, {Trader}. Welcome to AiX Bot. ' + emoji.get('rocket'),
        emoji.get('cop') + ' In order to use this bot you need to be registered as a trader. To register, please '+ emoji.get('email') +' your:',
        'Full Name\nTelegram Id\nEmail address\nto our address: ' + utils.AIX_REGISTRATION_EMAIL,
        'For your Telegram Id, just type: /info'
      ]
    }

    if(update.message.text == BOT_COMMANDS.NEW_SESSION) {
      if(update.session && update.session.cookiejar){
        update.session.cookiejar = null;
      }

      let wasAuthenticated = !!update.session.token;
      if(update.session) {
        update.session = {}
      }
      if (wasAuthenticated) {
        update.outputMessage = "New session started";
      }
      update.session.skip_bot_api = true;
    }

    if(update.message.text == BOT_COMMANDS.USER_INFO){
      update.session.skip_bot_api = true;
      update.outputMessage = [
        "{Trader}, here is your Telegram ID & Username:",
        "Telegram Id: " + update.sender.id,
        "Username: " + ((update.raw.message.from.username) ? update.raw.message.from.username : 'Not set')
      ];
    }

    next();
  }
};

module.exports = {
  helloMiddleware
}
