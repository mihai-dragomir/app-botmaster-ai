var appRoot = require('app-root-path');
var rp = require('request-promise');
var setCookie = require('set-cookie-parser');
var tough = require('tough-cookie');
const aixBotApiSettings = require(appRoot + '/config/config-bot-api');
const aixBotWrapper = require(appRoot + '/services/bot-api-wrapper');

const aixBotApiMiddleware = {
  type: 'incoming',
  name: 'send-message-to-bot-api-middleware',
  controller: (bot, update, next) => {
    if(update.session.skip_bot_api) {
      return next();
    }

    let authPromise = Promise.resolve();

    if (!update.session.token) {
      authPromise = aixBotWrapper
        .authenticateTrader(update)
        .then(response => {
          update.session.token = response.token; // store token

          return Promise.resolve();
        })
        .catch(err => {
          if (err.statusCode === 401 || err.label === 'missing_id') {
            update.outputMessage = 'You are unauthorised to use this bot. ' +
              'In order to register for this service please write the ' +
              'command /start and follow the instructions there.';
            update.session.lastAuthRequestAt = Date.now();
          } else if (err.label === 'too_frequent_calls') {
            update.outputMessage = 'Please read the instructions above.';
          } else {
            update.outputMessage = 'Our service is not available at the ' +
              'moment.';
          }

          next();

          return Promise.reject(err);
        })
    }

    authPromise
      .then(() => aixBotWrapper.sendBotMessage(bot, update))
      .then(parsedBody => {
        console.log(parsedBody);
        console.log(parsedBody.outputMessage);

        update.session.watsonUpdate = parsedBody.watsonUpdate;
        update.apiResponse = parsedBody;
        update.outputMessage = parsedBody.outputMessage;

        next();
      })
      .catch(err => {
        if (err.statusCode === 401) {
          delete update.session.token;
        } else {
          console.error(err);
        }
      });
  }
};

module.exports = {
  aixBotApiMiddleware,
}
