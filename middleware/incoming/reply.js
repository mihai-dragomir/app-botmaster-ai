var appRoot = require('app-root-path');
const utils = require("../../utils.js");
var _ = require('lodash');
var rp = require('request-promise');
const aixBotApiSettings = require(appRoot + '/config/config-bot-api');
var telegramBotAPI = require('../../services/telegram-bot-api-wrapper');
const telegramSettings = require('../../config/config-telegram');

const replyToUser = {
  type: 'incoming',
  name: 'reply-to-user-middleware',
  controller: (bot, update) => {
    console.log("> Middleware - replyToUser: ");

    if (update.debugMessage){
      return bot.reply(update, update.debugMessage);
    }else if(update.addBugMessage){
      return bot.reply(update, update.addBugMessage);
    }else if(update.session.isGroupChat){
      const regex = /\/([^\s^@.]*)(@{0,1})([a-z_]*)/g;
      let m = regex.exec(update.message.text);
      // [ '/price_btc@bot_username',
      // 'price_btc',
      // '@',
      // 'bot_username',
      // index: 0,
      // input: '/price_btc@bot_username' ]

      let groupUpdateMessage;
      if(m != null){
        switch (m[1]) {
          case "price_btc":
            var options = {
                method: 'POST',
                uri: aixBotApiSettings.AIX_BOT_API + 'group-chats/info-card',
                body: {
                  message: m[1],
                  group_name: update.raw.message.chat.title,
                  access_token: aixBotApiSettings.AIX_BOT_MASTER_KEY,
                },
                json: true
            };

            return rp(options)
              .then(function (parsedBody) {
                let obj = JSON.parse(parsedBody);
                let botmasterAddedMessages = [
                  'Hi, {Trader}. Here is the latest Bitcoin (BTC) update. If you want to trade click: https://t.me/' + telegramSettings.telegramBotUsername
                ];
                let messagesCascade = obj.message ? botmasterAddedMessages.concat(obj.message) : botmasterAddedMessages;

                obj.imgUrl && telegramBotAPI.sendPhoto(update.raw.message.chat.id, 'Source: CryptoCompare.com', obj.imgUrl);

                return bot.sendTextCascadeTo(
                  messagesCascade,
                  update.raw.message.chat.id
                );
              })
              .catch(function (err) {
                  console.error(err);
              });
            break;

          default:
            groupUpdateMessage = ['Sorry, I can\'t understand. Try to use the command /price_btc'];
            return bot.sendTextCascadeTo(groupUpdateMessage, update.raw.message.chat.id);
        }
      }
    }else {
      if(_.isArray(update.outputMessage)){
        return bot.sendTextCascadeTo(update.outputMessage, update.sender.id);
      }else {
        return bot.reply(update, update.outputMessage);
      }
    }
  }
};

module.exports = {
  replyToUser
}
