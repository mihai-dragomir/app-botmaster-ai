var request = require('request');
var appRoot = require('app-root-path');
var qs = require("querystring");
var http = require("https");
var rp = require('request-promise');
const Utils = require("../../utils.js");
const aixBotApiSettings = require('../../config/config-bot-api');
var setCookie = require('set-cookie-parser');
var tough = require('tough-cookie');
const aixBotWrapper = require(appRoot+'/services/bot-api-wrapper');

const saveBug = {
  type: 'incoming',
  name: 'save-bug',
  controller: (bot, update, next) => {
    console.log("> BUG");
    if (update.message.text.startsWith('/bug')){
      aixBotWrapper.saveBug(bot, update)
      .then((parsedBody)=>{
        console.log(parsedBody);
        update.addBugMessage = "The bug was saved. Thank you.";
        next();
      })
      .catch(err =>{
        console.error(err);
        update.addBugMessage = "Sorry, it seems we have an error. Please try again.";
      })

    }else {
      next();
    }
  }
};

module.exports = {
  saveBug,
}
