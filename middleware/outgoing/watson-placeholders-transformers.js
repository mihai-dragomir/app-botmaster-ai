var request = require("request");
var rp = require('request-promise');
let BSHolder = require('../../services/BlackScholes.js').BSHolder;
var BS = require('../../services/BlackScholes.js').BS;
var telegramBotAPI = require('../../services/telegram-bot-api-wrapper')

function replacePlaceholder(text, message, update){
  const regex = /\{.*?\}/g;
  let m;

  while ((m = regex.exec(text)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
          regex.lastIndex++;
      }

      m.forEach((match, groupIndex) => {
        console.log(`Found match, group ${groupIndex}: ${match}`);

        if (match.toLowerCase() === '{trader}') {
          message.message.text = message.message.text.replace(match, update.raw.message.from.first_name);
          return;
        }

        try {
          let obj = JSON.parse(match);
          if (obj.imgUrl) {
            message.message.text = message.message.text.replace(match, '');
            telegramBotAPI.sendPhoto(update.raw.message.chat.id, 'Source: CryptoCompare.com', obj.imgUrl);
          }
        } catch (err) {
          console.log('Failed to parse match serialised json: ', match);
        }
      });
  }
}

const replaceWatsonPlaceholders = {
  type: 'outgoing',
  name: 'watson-placeholder-outgoing-middleware',
  controller: (bot, update, message, next) => {
    // message will now be of type OutgoingMessage
    console.log("my-outgoing-middleware");
    const text = message.message.text;

    if(text.indexOf('{') >= 0){
      replacePlaceholder(text, message, update, next);
    }

    next();
  }
};

module.exports = {
  replaceWatsonPlaceholders
}
