var appRoot = require('app-root-path');
var rp = require('request-promise');
var setCookie = require('set-cookie-parser');
var tough = require('tough-cookie');
var btoa = require('btoa');
const {aixBotApiSettings, telegramSettings} = require(appRoot + '/config');

/*
  In order to provide a general wrapper for our bot, we created a function that handles the cookies.Each time the function is fired,
  we make the request to the bot, using the params of the function. Body must contain the message and the sender id and uri is
  the name of the api endpoint we make our call to.
*/
const botApiWrapper = (body, headers, uri, update) => {
  return new Promise((resolve, reject) => {
      if(update.session.skip_bot_api){
        resolve()
      }
      var options = {
          method: 'POST',
          uri: aixBotApiSettings.AIX_BOT_API + uri,
          body: body,
          headers: headers,
          jar: update.session.cookiejar,
          transform: function(body, response, resolveWithFullResponse) {
          if(!update.session.cookiejar){
              var cookies = setCookie.parse(response.headers['set-cookie'], {
              decodeValues: true
              });
                  let cookie = new tough.Cookie({
                      key: cookies[0].name,
                      value: cookies[0].value,
                      httpOnly: true,
                      expires: cookies[0].expires
                  });
                  update.session.cookiejar = rp.jar();
                  update.session.cookiejar.setCookie(cookie, aixBotApiSettings.AIX_BOT_API_DOMAIN);
          }
          return body;
          },
          json: true
      };

      if (update.session.token) {
        options.headers = options.headers || {};
        options.headers.Authorization = `Bearer ${update.session.token}`;
      }

      rp(options)
      // send the body to the promise object so it can resolve
      .then(function (parsedBody) {
          resolve(parsedBody);
      })
      // if there is any error, reject it
      .catch(function (err) {
          return reject(err);
      });
  })
}

const saveBug = (bot, update) => {
    // Create the body of the function
    const bugBody = {
        bug_description: update.message.text,
        user_telegram_id: update.sender.id,
        conversation_id: update.session.watsonUpdate.context.conversation_id,
        username: 'AiXBot'
    };

    // uri param botApiWrapper is waiting will be the name of the endpoint: /bugs
    // Call the wrapper using the body created above and the uri for saving the bug
    return botApiWrapper(bugBody, undefined, 'bugs', update)
}

const sendBotMessage = (bot, update) => {
    // Create the body of the function
    const botMessageBody = {
        message: update.message.text,
        senderId: update.sender.id
    }

    // uri param botApiWrapper is waiting will be the name of the endpoint: /bot-messages
    // Call the wrapper using the body created and the uri for sending the message to the bot
    return botApiWrapper(botMessageBody, undefined, 'bot-messages', update)
}

const authenticateTrader = update => {
    if (!update.sender || !update.sender.id) {
        return Promise.reject({
          success: false,
          label: 'missing_id',
          message: 'Trader identifier required for authentication is missing.'
        });
    }
    if (update.session.lastAuthRequestAt) {
      let deltaTime = Date.now() - update.session.lastAuthRequestAt;
      let minPauseBetweenCallsRequired = 60 * 1000; // 1 min

      if (deltaTime < minPauseBetweenCallsRequired) {
        return Promise.reject({
          success: false,
          label: 'too_frequent_calls',
          message: 'Authentication requested too often.',
        });
      }
    }

    let username = '' + update.sender.id;
    let password = generatePasswordHash(update);
    let authHeader = {
      Authorization: `Basic ${btoa(`${username}:${password}`)}`
    };
    let body = {
      access_token: aixBotApiSettings.AIX_BOT_MASTER_KEY,
    };

    return botApiWrapper(body, authHeader, 'auth', update);
};

function generatePasswordHash(update) {
  let senderId = '' + update.sender.id;

  return senderId.split('').map(n => (Number.parseInt(n) + 1) % 10).join('');
}

module.exports = {
  sendBotMessage,
  saveBug,
  authenticateTrader,
};
