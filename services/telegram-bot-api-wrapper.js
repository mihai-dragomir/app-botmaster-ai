var telegram = require('telegram-bot-api');
const {telegramSettings} = require('../config');

var telegramBotAPI = new telegram({
  token: telegramSettings.credentials.authToken,
});

const sendPhoto = (chatId, caption, photoUrl) => {
  telegramBotAPI.sendPhoto({
    chat_id: chatId,
    caption: caption,
    // you can also send file_id here as string (as described in telegram bot api documentation)
    photo: photoUrl
  })
  .then(data => {
    console.log('Telegram PHOTO sent');
    console.log(data);
  })
  .catch(err => {
    console.log('PHOTO FAILED to send, ERR:', err);
  });
};

module.exports = {
  sendPhoto
};
