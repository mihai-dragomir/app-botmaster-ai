# app-botmaster-ai

* Botmaster component for the AiX project.
* This app is based on [Botmaster](https://github.com/botmasterai/botmaster)

## Dependencies

_This project uses:_
* [nodeJS](https://nodejs.org/en/)
* A Telegram Bot. Use [BotFather](https://core.telegram.org/bots#6-botfather) to create one.
* [AiX Bot Api](https://bitbucket.org/mihai-dragomir/aix-bot-api/src/e033f5d9cbb3112aaf3f3e096c5defb503207b8f/?at=develop) setup locally.
* When playing locally, you will also need [localtunnel](https://localtunnel.github.io/www/) in order to make your local Botmaster server public on the web and connect it to Telegram.

## Playing locally

Create a Telegram Bot using [BotFather](https://core.telegram.org/bots#6-botfather)

Make sure you alreay have [AiX Bot Api](https://bitbucket.org/mihai-dragomir/aix-bot-api/src/e033f5d9cbb3112aaf3f3e096c5defb503207b8f/?at=develop) setup locally and is running.

Clone this repository locally

Copy `.env.example` to `.env`  
In your `.env` file replace the value of `TELEGRAM_TOKEN` with the AUTH Token of the bot you created at step 1.  
You can find the Auth Token of your bot using BotFather.

Update the values of ```AIX_BOT_API_DOMAIN``` and ```AIX_BOT_API``` with the values of you loccal AiX Bot API server if you changed the default values when setting up.

Install npm dependencies (run  ```npm install```  in the root folder of the project.)

Then, run the server in development mode. You should see the bellow message if everything is ok. The server by default uses port 3000.

//FOR WINDOWS:
Open file ```package.json``` with a text editor;
Modify the *dev* property of the *scripts* object as: ``` "set NODE_ENV=local && nodemon -i  index.js"```
Run the command: ```npm run dev```

//FOR MAC:

```bash
$ npm run dev
[nodemon] starting `node index.js`
```

Now your Botmaster server is alive but you need to connect it to the Telegram Bot. For this you need to install [localtunnel](https://localtunnel.github.io/www/) in order to make your local Botmaster server public on the web and connect it to Telegram.

Now request a tunnel to your local Botmaster server in another terminal instance. By default AiX Botmaster uses port 3000.
```bash
$ lt --port 3000
your url is: https://stolkfkwfb.localtunnel.me
```

Now connect your local Botmaster server to the actual Telegram Bot. This requires you to make the following request outside of Botmaster (using curl for instance or a browser):
```
https://api.telegram.org/bot<Auth Token Of Your Bot Created At Step 1>/setWebhook?url=<Your LocalTunnel URL from Step 10>/telegram/webhook1234
```
The final URL should be something similar to this:
```
https://api.telegram.org/bot505951728:AAErYgdq9wmTiHS8z5pgTG9jqScXCBbi5Dk/setWebhook?url=https://asylmfnpbf.localtunnel.me/telegram/webhook1234
```
If you make this request in a browser you should see something similar to this:
```
{"ok":true,"result":true,"description":"Webhook is already set"}
```

Now you are all setup. Open telegram, search for the  bot you created at Step 1 and have fun :).

## Having fun

Start conversating with your bot by typing ```/new```

### Scenarios

1)
Trader: Feb 55c
AiX: Hi Trader, do you need a delta hedge for this trade?
Trader: y (yes, yep, yeah etc)
AiX: Ok, what size are you looking for?
Trader: 1m
AiX: The price for 1 million barrels of February 2018 $55.00 strike WTI Crude Calls is $2.07/$2.12 basis current February 2018 WTI Futures price of $56.56
Trader: sell it
AiX: That's confirmed, you sell 1 million barrels of February 2018 $55.00 strike WTI Crude Calls at $2.07 and buy 670,000 barrels of February 2018 WTI Futures price at $56.56. Thank you for the business.

2)
Trader: Feb 55c
AiX: Hi Trader, do you need a delta hedge for this trade?
Trader: yep
AiX: Ok, what size are you looking for?
Trader: 1m
AiX: The price for 1 million barrels of February 2018 $55.00 strike WTI Crude Calls is $2.07/$2.12 basis current February 2018 WTI Futures price of $56.56
Trader: offer 2.1
AiX: Working an offer of $2.10 for you in 1 million barrels of February 2018 $55.00 strike WTI Crude Calls with a 67% delta hedge in February 2018 WTI Futures price at $56.56
(Market moves up and trade is executed)
AiX: That's confirmed, you sell 1 million barrels of February 2018 $55.00 strike WTI Crude Calls at $2.10 and buy 670,000 barrels of February 2018 WTI Futures price at $56.56. Thank you for the business.

3)
Trader: Feb 55c
AiX: Hi Trader, do you need a delta hedge for this trade?
Trader: No
AiX: Ok, what size are you looking for?
Trader: 1m
AiX: The price for 1 million barrels of February 2018 $55.00 strike WTI Crude Calls is $2.07/$2.12 basis current February 2018 WTI Futures price of $56.56
Trader: sell it
AiX: That's confirmed, you sell 1 million barrels of February 2018 $55.00 strike WTI Crude Calls at $2.07. Thank you for the business.