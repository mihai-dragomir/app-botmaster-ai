const express = require('express');
var bodyParser = require('body-parser')

function createExpressApp() {
  const app = express();
  
  // for parsing incoming request bodies in a middleware
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())

  return app;
}

module.exports = {
  express: createExpressApp
}
