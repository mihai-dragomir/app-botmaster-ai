module.exports = {
    USER_TYPE_TRADER        : 'TRADER',
    USER_TYPE_MARKET_MAKER  : 'MARKET_MAKER',
    CURRENCY_SYMBOL         : '$',
    MARKET_MAKERS           : [491337682, 499418762],
    AIX_REGISTRATION_EMAIL  : 'cristin.iosif@heron.ai',
    BOT_COMMANDS            : {
      START: '/start',
      NEW_SESSION: '/new',
      USER_INFO: '/info'
    }
}
