const isGroupChat = update => {
  return (update.raw.message.chat.type === 'group');
}

const telegramBotWebhookUrlFn = (botAuthToken, botWebhookEndpoint, localUrl) => {
  return `https://api.telegram.org/bot${botAuthToken}/setWebhook?url=${localUrl}/telegram${botWebhookEndpoint}`;
}

module.exports = {
  isGroupChat,
  telegramBotWebhookUrlFn
}
