const actions = {
  hi: {
      controller: () => 'Hi there.'
  },
  bye: {
      controller: () => 'bye please come again'
  },
  buttons: {
    controller: (params) => {
        const buttonTitles = params.content.split('|');
        var buttonsArray = [];
        for (const buttonTitle of buttonTitles) {
          buttonsArray.push({
            text: buttonTitle,
            payload: buttonTitle
          });
        }
        let recipientId = params.update ? params.update.sender.id : params.message.recipient.id;

        params.bot.sendDefaultButtonMessageTo(buttonsArray, "",  recipientId);

        return '';
    }
  }
};

module.exports = {
  actions
}
